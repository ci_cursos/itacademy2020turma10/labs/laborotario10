﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace laborotario10
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Pessoa> pessoas = new List<Pessoa>{
                new Pessoa{Nome="Ana", DataNascimento=new DateTime(1980,3,14), Casada=true},
                new Pessoa{Nome="Paulo", DataNascimento=new DateTime(1978,10,23), Casada=true},
                new Pessoa{Nome="Maria", DataNascimento=new DateTime(2000,1,10), Casada=false},
                new Pessoa{Nome="Carlos", DataNascimento=new DateTime(1999,12,12), Casada=true}
            };

            //Todas as pessoas casadas
            /*
            List<Pessoa> casadas = new List<Pessoa>();
            foreach (var p in pessoas)
            {
                if (p.Casada)
                {
                    casadas.Add(p);
                }
            }
            casadas.ForEach(p => Console.WriteLine(p));
            */

            //Todas as pessoas casadas via LINQ
            /*
            var casadasLinq = from p in pessoas
                              where p.Casada
                              select p;
            
            foreach(var p in casadasLinq)
            {
                Console.WriteLine(p);
            }

            //Todas as pessoas casadas via LINQ
            var casadasLinqV2 = pessoas.Where(p => p.Casada);
            Console.WriteLine(casadasLinqV2.Count());
            pessoas[0].Casada = false;
            Console.WriteLine(casadasLinqV2.Count());
            */

            //Todas as pessoas casadas que nasceram após 01/01/1980
            var r = pessoas.Where(p => p.Casada && p.DataNascimento >= new DateTime(1980,1,1));
            r.ToList().ForEach(p => Console.WriteLine(p));

            //Todos os nomes das pessoas casadas
            var r2 = pessoas
                    .Where(p => p.Casada)
                    .Select(p => p.Nome);
            r2.ToList().ForEach(n => Console.WriteLine(n));

            //Todos os nomes e data de nascimento das pessoas solteiras
            var r3 = pessoas
                    .Where(p => !p.Casada)
                    .Select(p => new {p.Nome, p.DataNascimento});
            r3.ToList().ForEach(o => Console.WriteLine($"{o.Nome} {o.DataNascimento:d}"));
        }
    }
}
